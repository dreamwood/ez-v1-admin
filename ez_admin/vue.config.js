const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    publicPath: "/",
    assetsDir: "admin",
    productionSourceMap: false,
    configureWebpack: {
        resolve: {
            fallback: {path: false}
        }
    },
    lintOnSave: false,

    devServer: {
        port: 1422, // 启动端口
        host: "127.0.0.1",
        open: true,  // 启动后是否自动打开网页
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        proxy: {
            '/api': {
                target: 'http://127.0.0.1',//代理地址，这里设置的地址会代替axios中设置的baseURL
                changeOrigin: true,// 如果接口跨域，需要进行这个参数配置
                pathRewrite: {
                    '^/api': '/'
                }
            }
        }
    },
})
