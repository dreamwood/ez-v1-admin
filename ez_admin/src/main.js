import Vue from 'vue'
import App from './App.vue'
import './assets/js/use'

import ezws from "@/assets/ws/ezws";

ezws.init()

Vue.config.productionTip = false

//自动化路由
import Router from 'vue-router'
Vue.use(Router)
import pagesRegister from "./assets/js/route";
let router = new Router({
  mode:"history",
  base:"admin",
  routes: pagesRegister
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
