import EzCommConfig from "@/comps/form/EzCommConfig";
import EzCommApi from "@/comps/form/EzCommApi";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzRadio",

        choices:[],
        setChoices(choices){this.choices = choices;return this},
        addChoice(label,value){this.choices.push({label,value});return this},

        showInRow:true,
        setShowInRow(isInRow){this.showInRow = isInRow;return this},
        setShowInLines(){this.showInRow = false;return this},

         ...EzCommApi(),
    }
}