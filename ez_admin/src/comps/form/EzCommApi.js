export default function () {
return{
    api:"",
    setApi(api){this.api=api;return this},

    apiParams:{},
    setApiParams(param){this.apiParams = param;return this},

    apiCbFunc(data){return data},
    setApiCbFunc(func){this.apiCbFunc = func;return this},

    apiWhere:{},
    setApiWhere(apiWhere){this.apiWhere = apiWhere;return this},

    apiPage:1,
    setApiPage(apiPage){this.apiPage = apiPage;return this},

    apiLimit:-1,
    setApiLimit(apiLimit){this.apiLimit = apiLimit;return this},

    apiOrder:10,
    setApiOrder(apiOrder){this.apiOrder = apiOrder;return this},

    apiSearchKeys:[],
    setApiSearchKeys(apiSearchKeys){this.apiSearchKeys = apiSearchKeys;return this},
}
}