import {createSocket, sendWSPush} from "@/assets/ws/ws";

export default {
    businessType:{
        reg:0,
        logout:2,
        comm:4,

    },

    //创建基础信息载体
    createMessage : ()=>{
        return {
            business:0,
            content:""
        }
    },

    init(){

        createSocket(`ws://${location.host}/ws/websocket`)

        //window.addEventListener('onmessageWS', this.getSocketData)

    }
}