import theme from 'muse-ui/lib/theme';
theme.add('teal', {
    primary: '#009688',
    secondary: '#ff4081',
    success: '#4caf50',
    warning: '#ffeb3b',
}, 'light');


theme.use("teal");