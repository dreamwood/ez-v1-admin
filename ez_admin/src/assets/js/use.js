import Vue from 'vue';

Vue.prototype.$env = process.env

import MuseUI from 'muse-ui';
import 'muse-ui/dist/muse-ui.css';

Vue.use(MuseUI);

import '../css/sumall.css'

//登录页粒子动效
import VueParticles from 'vue-particles'
Vue.use(VueParticles)

//吐司消息
import Toast from 'muse-ui-toast';
Vue.use(Toast,{
    position:"top"
});

//弹窗消息
import Message from 'muse-ui-message';
Vue.use(Message);

import store from './ez-store'
Vue.prototype.$ezstore = store

//加载网络请求
import http from "@/assets/js/http"
http.callbackError = function(res){
    if (process.env["ENV "] === "development"){
        // console.log(res)
    }
    if (res.data !== undefined && res.data.code>=4000){
        Toast.message(res.data.message)
        return false
    }
    return true
}
http.callbackCatch = function(err){
    // console.log(err)
}
Vue.prototype.$http = http

//加载通用混入
import mixins from "@/assets/js/mixins";
Vue.mixin(mixins.system)

//工具包
import utils from "@/assets/js/utils";
Vue.prototype.$utils = utils

import vColorPicker from 'vcolorpicker'
Vue.use(vColorPicker)