import com from "@/assets/js/stores/com";

export default {
    //本地存储key
    key:"ez.auth",
    //实际数据
    data: {
        token:"",
        isLogin: false,
        user:{},
        //模块权限
        models:[],
        //页面权限
        menus:[],
        pages:{},
    },
    //数据是否准备完成，在启动或者重载时，需要从本地获取数据
    dataReady:false,

    saveToLocal(){
        this.saveLoc(this.key,this.data)
    },
    createFromLocal(){
        let tmp = this.getLoc(this.key)
        if (!tmp){
            //this.data = tmp
        }else {
            this.data = tmp
        }

        this.dataReady = true
    },

    setIsLogin(login) {
        this.data.isLogin = login
        this.saveToLocal()
    },
    getIsLogin(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.isLogin
    },

    setUser(v) {
        this.data.user = v
        this.saveToLocal()
    },
    getUser(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.user
    },

    setToken(v) {
        this.data.token = v
        this.saveToLocal()
    },
    getToken(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.token
    },

    setModels(v) {
        this.data.models = v
        this.saveToLocal()
    },
    getModels(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.models
    },

    setMenus(v) {
        this.data.menus = v
        this.saveToLocal()
    },
    getMenus(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.menus
    },

    setTops(v) {
        this.data.tops = v
        this.saveToLocal()
    },
    getTops(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.tops
    },

    setPages(v) {
        this.data.pages = v
        this.saveToLocal()
    },
    getPages(){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data.pages
    },

    checkFunc(authKey){
        return authKey
    },


    ...com
}