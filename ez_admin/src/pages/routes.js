export default [
  {
    path:"",
    name:"root",
    componentPath: "./layout.vue",
    children:[
      {
        path:'index',
        name:"index",
        componentPath: "./index.vue",
        children:[

        ]
      },
      {
        path:'micro_*',
        name:"micro",
        componentPath: "./index.vue",
      },
      {
        path:'dev_*',
        name:"dev_",
        componentPath: "./index.vue",
      },
      {
        path:'prod_*',
        name:"prod_",
        componentPath: "./index.vue",
      },
    ]
  },
  {
    path:'/login',
    name:"login",
    componentPath: "./login.vue",
  },
]
