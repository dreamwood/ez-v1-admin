const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    publicPath: process.env.VUE_APP_EZ_MICRO_PUBLIC_PATH,
    productionSourceMap: false,
    configureWebpack: {
        resolve: {
            fallback: {path: false}
        },
        output: {
            library: `micro-[name]`,
            libraryTarget: 'umd', // 把微应用打包成 umd 库格式
            // jsonpFunction: `webpackJsonp_micro`,
        },
    },
    lintOnSave: false,
    devServer: {
        port: 19001, // 启动端口
        host: "127.0.0.1",
        open: true,  // 启动后是否自动打开网页
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        proxy: {
            '/api': {
                target: 'http://127.0.0.1:8080',//代理地址，这里设置的地址会代替axios中设置的baseURL
                changeOrigin: true,// 如果接口跨域，需要进行这个参数配置
                pathRewrite: {
                    '^/api': '/api'
                }
            }
        }
    },
})
