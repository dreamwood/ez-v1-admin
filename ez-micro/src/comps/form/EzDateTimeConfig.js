import EzCommConfig from "@/comps/form/EzCommConfig";
export default function () {
    return{
        ...EzCommConfig(),
        type:"EzDateTime",

        //date/time/year/month/dateTime
        showType:"dateTime",
        setDateTypeDate(){this.showType = "date"; return this},
        setDateTypeTime(){this.showType = "time"; return this},
        setDateTypeYear(){this.showType = "year"; return this},
        setDateTypeMonth(){this.showType = "month"; return this},
    }
}