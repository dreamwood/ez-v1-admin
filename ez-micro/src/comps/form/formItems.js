import EzArt from '@/comps/form/EzArt'
import EzText from '@/comps/form/EzText'
import EzBlank from "@/comps/form/EzBlank";
import EzRadio from "@/comps/form/EzRadio";
import EzCheckBox from "@/comps/form/EzCheckBox";
import EzSwitch from "@/comps/form/EzSwitch";
import EzSelect from "@/comps/form/EzSelect";
import EzDateTime from "@/comps/form/EzDateTime";
import EzCascade from "@/comps/form/EzCascade";
import EzFiles from "@/comps/form/EzFiles";
import EzSearch from "@/comps/form/EzSearch";
import EzColor from "@/comps/form/EzColor";
import EzTreeSelect from "@/comps/form/EzTreeSelect.vue";

export default {
    EzArt,
    EzText,
    EzBlank,
    EzRadio,
    EzCheckBox,
    EzSwitch,
    EzSelect,
    EzDateTime,
    EzCascade,
    EzFiles,
    EzSearch,
    EzColor,
    EzTreeSelect,
}