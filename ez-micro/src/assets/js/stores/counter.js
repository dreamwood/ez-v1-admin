import com from "@/assets/js/stores/com";

export default {
    //本地存储key
    key:"ez.count",
    //实际数据
    data: {

    },
    //数据是否准备完成，在启动或者重载时，需要从本地获取数据
    dataReady:false,

    saveToLocal(){
        this.saveLoc(this.key,this.data)
    },
    createFromLocal(){
        let tmp = this.getLoc(this.key)
        if (!tmp){
            //this.data = tmp
        }else {
            this.data = tmp
        }

        this.dataReady = true
    },

    set(key,value) {
        this.data[key] = value
        this.saveToLocal()
    },
    get(key){
        if (!this.dataReady){
            this.createFromLocal()
        }
        return this.data[key]
    },


    checkFunc(authKey){
        return authKey
    },


    ...com
}