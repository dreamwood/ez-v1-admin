import axios from "axios"


export default {
    authKey: "ez-auth-token",
    accessKey: "ez-access-token",
    getCoreUrl() {
        return process.env.VUE_APP_CORE_API
    },
    getRootUrl() {
        return process.env.VUE_APP_BASE_API
    },
    root() {
        return this.getRootUrl()
    },
    baseUrl() {
        return this.getRootUrl()
    },
    getUrl() {
        return this.getRootUrl()
    },
    callbackError: function () {
        return true
    },
    callbackCatch: function () {
    },
    getAuthToken() {
        return localStorage.getItem(this.authKey)
    },
    setAuthToken(v) {
        if (v === undefined) {
            return false
        }
        localStorage.setItem(this.authKey, v)
    },
    getAccessToken() {
        return localStorage.getItem(this.accessKey)
    },
    setAccessToken(v) {
        if (v === undefined) {
            return false
        }
        localStorage.setItem(this.accessKey, v)
    },
    getHeader() {
        let header = {
            'Content-Type': 'application/json',
        }
        if (this.getAuthToken()) {
            header[this.authKey] = this.getAuthToken()
        }
        if (this.getAccessToken()) {
            header[this.accessKey] = this.getAccessToken()
        }
        return header
    },
    get(url, data, func) {
        let _this = this
        axios({
            method: 'GET',
            url: _this.getUrl() + url,
            params: data,
            headers: _this.getHeader()
        }).then(function (res) {
            _this.setAuthToken(res.headers[`set-${_this.authKey}`])
            _this.setAccessToken(res.headers[`set-${_this.accessKey}`])
            if (!_this.callbackError(res)) return
            func(res.data)
        }).catch(function (error) { // 请求失败处理
            _this.callbackCatch(error)
        })
    },
    post(url, data, func, method) {
        let _this = this
        if (method === undefined) {
            method = "POST"
        }
        axios({
            method: method,
            url: _this.getUrl() + url,
            data: data,
            headers: _this.getHeader()
        }).then(function (res) {
            _this.setAuthToken(res.headers[`set-${_this.authKey}`])
            _this.setAccessToken(res.headers[`set-${_this.accessKey}`])
            if (!_this.callbackError(res)) return
            func(res.data)
        }).catch(function (error) { // 请求失败处理
            _this.callbackCatch(error)
        })
    },
    post_core(url, data, func, method) {
        let _this = this
        if (method === undefined) {
            method = "POST"
        }
        axios({
            method: method,
            url:  _this.getCoreUrl() + url,
            data: data,
            headers: _this.getHeader()
        }).then(function (res) {
            _this.setAuthToken(res.headers[`set-${_this.authKey}`])
            _this.setAccessToken(res.headers[`set-${_this.accessKey}`])
            if (!_this.callbackError(res)) return
            func(res.data)
        }).catch(function (error) { // 请求失败处理
            _this.callbackCatch(error)
        })
    },
    upload($file, func, dir) {
        let _this = this
        var formdata = new FormData()
        formdata.append('file', $file)
        if (dir !== undefined){
            formdata.append('dir',dir)
        }
        let url = '/upload'
        axios({
            url: _this.getUrl() + url,
            method: 'post',
            data: formdata,
            headers: {
                "Content-Type":"multipart/form-data"
            },
        }).then((res) => {
            func(res.data)
        }).catch(function (error) { // 请求失败处理
            _this.callbackCatch(error)
        })
    },
    post_out(url, data, func, method) {
        let _this = this
        if (method === undefined) {
            method = "POST"
        }
        axios({
            method: method,
            url: url,
            data: data
        }).then(function (res) {
            func(res)
        }).catch(function (error) { // 请求失败处理
            _this.callbackCatch(error)
        })
    },
}
