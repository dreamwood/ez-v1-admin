var props = null ;

export default {
    setProps(p){
        props = p
    },
    dispatch(state){
        props.setGlobalState(state);
    }
}
