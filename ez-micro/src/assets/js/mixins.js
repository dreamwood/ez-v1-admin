export default {
    system:{
        methods:{
            gotoLogin(){
                this.$router.replace("/login")
            },
            goto(url){
                this.$router.push(url)
            },
            goback(delta){
                if (delta === undefined) delta = -1
                this.$router.go(delta)
            }
        }
    }
}