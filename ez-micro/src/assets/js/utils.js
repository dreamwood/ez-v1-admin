export default {

    isObject(val) {
        return Object.prototype.toString.call(val) === '[object Object]' && val !== null && val !== undefined;
    },
    isArray(val) {
        return Object.prototype.toString.call(val) === '[object Array]';
    },
    isString(val) {
        return Object.prototype.toString.call(val) === '[object String]';
    },
    isDate(val) {
        return Object.prototype.toString.call(val) === '[object Date]';
    },
    isFunction(val) {
        return Object.prototype.toString.call(val) === '[object Function]';
    },
    isNumber(val) {
        return Object.prototype.toString.call(val) === '[object Number]';
    },
    isBoolean(val) {
        return Object.prototype.toString.call(val) === '[object Boolean]';
    },
    isAbsoluteURL(url) {
        return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
    },
    setLocData (key,val) {
        localStorage.setItem(key,JSON.stringify(val))
    },
    getLocData(key){
        try {
            return JSON.parse(localStorage.getItem(key))
        }catch (e) {
            return (localStorage.getItem(key))
        }
    },
    clearLocData(key){
        localStorage.removeItem(key)
    },
    clearAllLocData(){
        localStorage.clear()
    },
    str2time (str) {
        let date = new Date(str)
        return this.getDateTime(date)
    },
    str2timeShort (str) {
        let date = new Date(str)
        return  this.getDateTime(date).substr(5,11)
    },
    str2date (str) {
        let date = new Date(str)
        let datetime = this.getDateTime(date)
        return datetime.substr(0,10)
    },
    getDateTime (date) {
        if (date === undefined) {
            date = new Date()
        }else {
            if (this.isString(date)){
                date =  new Date(date)
            }
        }
        return date.getFullYear() + '-'
            + ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1))
            + '-' + date.getDate()
            + ' ' + date.getHours()
            + ':' + date.getMinutes()
            + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds())
    },

    objDelIndex(obj, index) {
        var tmp = {};
        for (var i in obj) {
            if (i != index) {
                tmp[i] = obj[i]
            }
        }
        return tmp;
    },

    objDelValue(obj, value) {
        var tmp = {};
        for (var i in obj) {
            if (obj[i] != value) {
                tmp[i] = obj[i]
            }
        }
        return tmp;
    },


    arrayDelIndex(array, index) {
        var tmp = [];
        array.map(function (data, i) {
            if (i != index) {
                tmp.push(data)
            }
        })
        return tmp;
    },

    arrayDelValue(array, value) {
        var tmp = [];
        array.map(function (data, i) {
            if (data != value) {
                tmp.push(data)
            }
        })
        return tmp;
    },

    isInArray(val, array) {
        if (!this.isTrue(array)) {
            return false
        }
        for (var i in array) {
            if (array[i] == val) {
                return true;
            }
        }
        return false;
    },
    arrayReplaceValue(array, find, replace) {
        var tmp = [];
        array.map(function (data, i) {
            if (data != find) {
                tmp.push(data)
            } else {
                tmp.push(replace)
            }
        })
        return tmp;
    },
}