import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("app","APP",80)
lb.add("dir","域",80)
lb.add("name","名称",80)
lb.add("fields","字段",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}