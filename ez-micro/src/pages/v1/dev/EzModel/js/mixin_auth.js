export default {
    directives:{
        auth:{
            bind($el,binding){
                this.$ezstore.auth.checkFunc(binding.value)
                $el.style.display = "none"
            }
        }
    }
}