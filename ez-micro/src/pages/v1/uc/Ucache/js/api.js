import http from "@/assets/js/http";

export default {
    urlFind: '/uc/admin/Ucache/get',
    urlFindToEdit: '/uc/admin/Ucache/get',
    urlFindBy: '/uc/admin/Ucache/list',
    urlSave: '/uc/admin/Ucache/save',
    //urlDelete: '/uc/admin/Ucache/delete',
    //urlDeleteAll: '/uc/admin/Ucache/delete_many',
    urlDelete: '/uc/admin/Ucache/destroy',
    urlDeleteAll: '/uc/admin/Ucache/destroy_many',
    urlCopy: '/uc/admin/Ucache/copy',
    urlEditMany: '/uc/admin/Ucache/edit_many',
    urlTree: '/uc/admin/Ucache/tree',
    urlChoice: '/uc/admin/Ucache/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}