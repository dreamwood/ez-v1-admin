import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("uid","UID",80)
lb.add("key","KEY",80)
lb.add("data","数据",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}