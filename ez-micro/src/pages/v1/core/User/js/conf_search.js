import formBuilder from "@/comps/form/formBuilder";

export default function () {
    let fb = formBuilder()
    fb.setLabelPosition("right").setLabelWidth(100)
    fb.addDateTime("updateAt__gt","更新时间",3)
    return fb
}