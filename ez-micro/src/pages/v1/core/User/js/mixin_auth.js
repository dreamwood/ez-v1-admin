import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_root: "erp:User",

            auth_add: ezAuth.reg("+core:User:add", "用户新增"),//新增
            auth_copy: ezAuth.reg("+core:User:copy", "用户复制"),//复制
            auth_edit: ezAuth.reg("+core:User:edit", "用户编辑"),//编辑
            auth_del: ezAuth.reg("-core:User:del", "用户删除"),//删除
            auth_view: ezAuth.reg("+core:User:view", "用户查看"),//查看

        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}