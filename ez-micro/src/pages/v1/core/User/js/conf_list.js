import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("account","账号",80)
lb.add("password","密码",80)
lb.add("salt","Salt",80)
lb.add("token","Token",80)
lb.add("pic","头像",80)
lb.add("name","姓名",80)
lb.add("code","工号",80)
lb.add("phone","手机号",80)
lb.add("idCard","身份证",80)
lb.add("sex","性别",80)
lb.add("age","年龄",80)
lb.add("roles","角色",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("account","账号",120)
    // lb.add("salt","Salt",80)
    // lb.add("pic","头像",80)
    lb.add("name","姓名",120)
    lb.add("code","工号",120)
    lb.add("phone","手机号",180)
    // lb.add("idCard","身份证",80)
    // lb.add("sex","性别",80)
    // lb.add("age","年龄",80)
    lb.add("token","Token",0)
    lb.add("reset","-",0)
    lb.add("updateAt","更新时间",180).setListIsDateTime()
    lb.addAction(120).setAlignRight()
    return lb.headers
}