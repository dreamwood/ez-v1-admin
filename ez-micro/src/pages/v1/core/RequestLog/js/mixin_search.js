import confSearch from "./conf_search";
export default {
    data(){
        //搜索条件表单配置，也可以使用类似于表单的json配置效果一样

        return{
            //是否显示筛选条件表单
            showSearch:true,
            searchText:"",
            isFocusSearch: false,//控制模糊搜索动画
            //筛选器
            conditions: {},
            searchFormConfig:confSearch(),
            searchFormUpdateKey: Math.random(),

            selected:[]
        }
    },
    methods:{
        focusSearch() {
            this.isFocusSearch = !this.isFocusSearch
        },
        search(){
            this.getList(1)
        },
        //重置查询条件
        resetSearchBox() {
            this.conditions = {}
            this.searchFormUpdateKey = Math.random()
            this.getList(1)
        },
        //缓存查询条件
    },
    watch: {
        showSearch() {
            if (this.showSearch) {
                this.showEdit = false
            }
        }
    }
}