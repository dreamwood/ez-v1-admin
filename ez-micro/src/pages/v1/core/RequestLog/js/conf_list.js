import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("url","",80)
lb.add("session","",80)
lb.add("logs","",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("_action","-",110)
    lb.add("id","#",80)
    lb.add("url","URL",380).setCopy()
    lb.add("createAt","创建时间",180).setListIsDateTime()
    lb.add("session","SESSION",0)
    lb.addAction(120).setAlignRight()
    return lb.headers
}