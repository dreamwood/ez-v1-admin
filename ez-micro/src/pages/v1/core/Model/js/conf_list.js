import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("name","模块名",80)
lb.add("cnName","中文名",80)
lb.add("entry","入口地址",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("appId","AppId",180)
    lb.add("name","模块名",180)
    lb.add("cnName","中文名",180)
    lb.add("entry","入口地址",0)
    lb.add("isOn","启用",180).setListIsSwitch()
    lb.addAction(120).setAlignRight()
    return lb.headers
}