import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
/*
fb.addText("name","模块名",3)
fb.addText("cnName","中文名",3)
fb.addText("entry","入口地址",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("appId","AppId",3)
        fb.addText("name","模块名",3)
        fb.addText("cnName","中文名",3)
        fb.addText("entry","入口地址",3)
        fb.addSwitch("isOn","启用",3)
        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    }
}