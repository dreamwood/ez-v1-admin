import confList from "./conf_list";

export default {
    data() {
        let headers = confList()
        return {
            tableKey: "t",
            //列表项原始配置
            headersOrg: confList(),
            //经过加工后实际生效的列表项配置
            headers: headers,
            headerConfigOpen:false,
        }
    },
    methods: {
        onHeaderConfigChange(k,v){
            this.headers = v
            this.tableKey = k
        }
    },
}