import confEdit from "./conf_edit";
import apis from "./api";

export default {
    components: {},
    props:{
        isComp:{//Edit页是否以组件的形式使用
            default:false,
        },
        modelId:{
            default: 0,
        },
        base:{},//数据新建的时候默认的值
    },
    data() {
        return {
            showForm: false,
            configEdit: confEdit,
            editKey: "",//model组建key值，更新回导致重新渲染
            url: '',//页面url
            showTool: false,//是否显示工具箱

            editTab: "main",

            //数据管理
            id: 0,
            model: {
                modelIds:[],
                topIds:[],
                menuIds:[],
                apiIds:[],
            },
        }
    },
    mounted(){
        if (this.isComp){
            //从属性中取id
            this.id = this.modelId
        }else {
            //从$router中取id
            this.id = this.$route.query.id * 1
        }
        if (this.id>0){
            this.getData()
        }else {
            this.model = this.base
        }
    },
    methods: {
        openEdit(id) {
            if (id > 0) {
                this.id = id
                //this.getData()
            } else {
                this.id = 0
                //this.model = this.base
            }
            this.showForm = true
        },
        closeEdit() {
            this.showForm = !this.showForm
            this.refresh()
        },
        cancelEdit(){
            if (this.isComp){
                this.$emit("done")
            }else {
                this.goback(-1)
            }
        },
        save() {
            this.$refs.editForm.valid().then(res => {
                if (res) {
                    apis.save(this.model, res => {
                        this.$toast.success(res.message)
                        this.showForm = false
                        this.cancelEdit()
                    })
                }
            })

        },
        getData() {
            apis.findToEdit(this.id, res => {
                this.model = res.data
                this.editKey = Math.random()
            })
        },
        saveInList(){
            this.$refs.editor.save()
        }
    },
    watch:{
        editTab(){
            this.editKey = Math.random()
        }
    }
}
