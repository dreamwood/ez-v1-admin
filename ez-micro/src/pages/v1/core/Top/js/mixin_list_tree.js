import apis from "./api";
export default {
    data(){
        return{
            page:1,
            limit:1000,
            group:"",
            total:0,
            //数据挖掘
            dumps:["model"],
            //筛选条件
            conditions:{},
            //排序
            order:["l"],
            //列表数据
            data:[],
            childCount:{},
            orgData:[],
            expandLinks:[""]
        }
    },
    mounted() {
        this.getList()
    },
    methods:{
        getList(page){
            if (page !== undefined) this.page = page
            let cond = {}
            for (let key in this.params) {
                cond[key] = this.params[key]
            }
            if (this.showSearch){
                for (let key in this.conditions) {
                    if (!this.conditions[key])continue
                    cond[key] = this.conditions[key]
                }
            }
            if (this.searchText !== ""){
                cond["_and"]={
                    _ors:[
                        {name__like: this.searchText},
                    ]
                }
            }
            let param = {
                _where: cond,
                _order: this.order,
                _dumps:this.dumps,
                page:this.page,
                limit:this.limit,
            }
            apis.findBy(param,  (data) => {
                this.orgData = data
                this.expandLinks = [""]
                this.createTree()
            })
        },


        refresh() {
            this.getList(1)
        },

        copy: function () {
            let ids = []
            for (let row of this.selected) {
                ids.push(this.data[row].id)
            }
            apis.copy(ids, res => {
                this.$toast.success(
                    res.message
                )
                this.getList(this.page)
                this.selected = []
            })
        },
        remove: function (id) {
            if (this.askBeforeDelete){
                this.$confirm('确定要进行删除操作吗？', '提示').then(({ result }) => {
                    if (result) {
                        apis.delete(id, (res)=>{
                            this.$toast.success(
                                res.message
                            )
                            this.getList(this.page)
                        })
                    } else {
                        this.$toast.message('取消删除');
                    }
                });
            }else {
                apis.delete(id, (res)=>{
                    this.$toast.success(
                        res.message
                    )
                    this.getList(this.page)
                })
            }
        },
        removeAll: function () {
            this.$confirm('确定要进行删除操作吗？', '提示').then(({ result }) => {
                if (result) {
                    let ids = []
                    for (let row of this.selected) {
                        ids.push(this.data[row].id)
                    }
                    apis.deleteAll(ids, (res)=>{
                        this.$toast.info(
                            res.message
                        )
                        this.getList(this.page)
                    })
                    this.selected = []
                } else {
                    this.$toast.message('取消删除');
                }
            });
        },
        createTree(){
            let tmp = []
            this.childCount = {}
            for (let item of this.orgData) {
                //找到展开项目并展示
                let find =false
                for (let link of this.expandLinks) {
                    if (link === item.link){
                        find = true
                    }
                }
                if (find){
                    tmp.push(item)
                }
                //记录所有项目的子代数量
                if (this.childCount[item.parentId] === undefined){
                    this.childCount[item.parentId] = 0
                }
                this.childCount[item.parentId]++
            }
            this.data = tmp
        },
        toggle(item){
            let cur= `${item.link}${item.l}_`
            if (!!item.open){
                //收起
                let tmp = []
                for (let link of this.expandLinks) {
                    if (!link.startsWith(cur)){
                        tmp.push(link)
                    }
                }
                this.expandLinks = tmp
                //收起后要重新更新一下open值
                for (let row of this.data) {
                    if (row.link.startsWith(cur)){
                        row.open = false
                    }
                }
                item.open= false
            }else {
                //展开
                this.expandLinks.push(cur)
                item.open= true
            }
            this.createTree()
        },
        expand(){
            this.data = this.orgData
            this.expandLinks=[""]
            for (let item of this.data) {
                this.expandLinks.push(item.link)
                item.open = true
            }
        },
        shrink(){
            this.expandLinks=[""]
            this.createTree()
            for (let item of this.orgData) {
                item.open = false
            }
            for (let item of this.data) {
                item.open = false
            }
        }
    }
}