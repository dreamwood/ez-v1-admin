import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Top/info',
    urlFindToEdit: '/core/admin/Top/get',
    urlFindBy: '/core/admin/Top/list',
    urlSave: '/core/admin/Top/save',
    urlDelete: '/core/admin/Top/delete',
    urlDeleteAll: '/core/admin/Top/delete_many',
    urlCopy: '/core/admin/Top/copy',
    urlEditMany: '/core/admin/Top/edit_many',
    urlTree: '/core/admin/Top/tree',
    urlChoice: '/core/admin/Top/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete,{id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}