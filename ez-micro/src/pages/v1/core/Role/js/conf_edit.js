import formBuilder from "@/comps/form/formBuilder";
import api from "./api";
import EzTreeSelectConfig from "@/comps/form/EzTreeSelectConfig";
import apiModel from "@/pages/v1/core/Model/js/apiModel";
import apiMenu from "@/pages/v1/core/Menu/js/apiMenu";
import apiTop from "@/pages/v1/core/Top/js/apiTop";
import apiApi from "@/pages/v1/core/Api/js/apiApi";
import apiPage from "@/pages/v1/core/Page/js/apiPage";
import EzCheckBoxConfig from "@/comps/form/EzCheckBoxConfig";
/*
fb.addText("name","角色名",3)
fb.addText("code","编码",3)

* */
export default {
    main:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addText("name","角色名",3)
        fb.addText("code","编码",3)

        return fb
    },
    tree:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100)

        fb.addCascade("parentId").setLabel("上级分类").setSpan(3)
            .setApi(api.urlTree)

        fb.addText("name").setSpan(6).setLabel("分类名称")
            .setRules([fb.R.notNull("名称不能为空")])

        fb.addText("sort").setSpan(3).setLabel("排序").setTypeNumber()
        return fb
    },
    other:function () {
        let fb = formBuilder()
        fb.setLabelWidth(100).setLabelPosition("right")

        return fb
    },
    menu(){
        let conf = EzTreeSelectConfig()
        conf.setApi(apiMenu.urlChoice).setApiParams({
            _order:["l"],
        })
        return conf
    },
    model(){
        let fb = formBuilder()
        fb.setLabelWidth(100)
        fb.addTree("modelIds").setSpan(12).setLabel("选择模块")
            .setApi(apiModel.urlChoice)
        return fb
    },
    top(){
        let conf = EzTreeSelectConfig()
        conf.setApi(apiTop.urlChoice).setApiParams({
            _order:["l"],
        })
        return conf
    },
    api(){
        let conf = EzTreeSelectConfig()
        conf.setApi(apiApi.urlChoice).setApiParams({
            _order:["l"],
        })
        return conf
    },
    page(){
        let conf = EzCheckBoxConfig()
        conf.setApi(apiPage.urlChoice).setApiParams({
            _order:["name"],
        })
        return conf
    },
}