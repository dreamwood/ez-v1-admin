import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("name","角色名",80)
lb.add("code","编码",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("name","角色名",180)
    lb.add("code","编码",180)

    lb.addAction(120).setAlignRight()
    return lb.headers
}