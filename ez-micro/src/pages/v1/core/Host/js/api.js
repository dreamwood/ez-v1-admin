import http from "@/assets/js/http";

export default {
    urlFind: '/core/admin/Host/info',
    urlFindToEdit: '/core/admin/Host/get',
    urlFindBy: '/core/admin/Host/list',
    urlSave: '/core/admin/Host/save',
    urlDelete: '/core/admin/Host/delete',
    urlDeleteAll: '/core/admin/Host/delete_many',
    urlCopy: '/core/admin/Host/copy',
    urlEditMany: '/core/admin/Host/edit_many',
    urlTree: '/core/admin/Host/tree',
    urlChoice: '/core/admin/Host/choice',
    urlReload: '/core/admin/Host/reload',
    reload(func) {
        http.get(this.urlReload, {}, res => {
            func(res)
        })
    },
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}