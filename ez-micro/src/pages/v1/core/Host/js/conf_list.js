import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("appId","APPID",80)
lb.add("machineCode","机器码",80)
lb.add("ip","IP地址",80)
lb.add("port","端口",80)
lb.add("weight","权重",80)
lb.add("isOn","启用",80)
lb.add("state","",80)
lb.add("stateInfo","",80)
lb.add("lastTime","上次心跳时间",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)

    lb.add("appId","APPID",120)
    lb.add("machineCode","机器码",100)
    lb.add("weight","权重",80)
    lb.add("ip","IP地址",120)
    lb.add("port","端口",180)
    lb.add("isOn","启用",80).setListIsSwitch()
    lb.add("state","状态码",80)
    lb.add("stateInfo","主机状态",0)
    lb.add("lastTime","上次心跳时间",180).setListIsDateTime()

    lb.addAction(120).setAlignRight()
    return lb.headers
}