import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("model","所属模块",80)
lb.add("name","名称",80)
lb.add("url","URL",80)
lb.add("icon","图标",80)
lb.add("sort","排序",80)
lb.add("l","L",80)
lb.add("r","R",80)
lb.add("level","Level",80)
lb.add("link","link",80)
lb.add("parent","父级菜单",80)
lb.add("children","子菜单",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.add("model","所属模块",180).setIsObjectName()
    lb.add("name","名称",320)
    lb.add("url","URL",0)
    lb.add("icon","图标",180)
    lb.add("sort","排序",80)
    lb.add("l","L",80)
    lb.add("r","R",80)
    lb.addAction(220).setAlignRight()
    return lb.headers
}