import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/Question/get',
    urlFindToEdit: '/xl/admin/Question/get',
    urlFindBy: '/xl/admin/Question/list',
    urlSave: '/xl/admin/Question/save',
    //urlDelete: '/xl/admin/Question/delete',
    //urlDeleteAll: '/xl/admin/Question/delete_many',
    urlDelete: '/xl/admin/Question/destroy',
    urlDeleteAll: '/xl/admin/Question/destroy_many',
    urlCopy: '/xl/admin/Question/copy',
    urlEditMany: '/xl/admin/Question/edit_many',
    urlTree: '/xl/admin/Question/tree',
    urlChoice: '/xl/admin/Question/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}