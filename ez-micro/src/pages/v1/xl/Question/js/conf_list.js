import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("title","问题标题",80)
lb.add("isMulti","是否多选",80)
lb.add("note","备注",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}