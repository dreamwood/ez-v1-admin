import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/Choice/get',
    urlFindToEdit: '/xl/admin/Choice/get',
    urlFindBy: '/xl/admin/Choice/list',
    urlSave: '/xl/admin/Choice/save',
    //urlDelete: '/xl/admin/Choice/delete',
    //urlDeleteAll: '/xl/admin/Choice/delete_many',
    urlDelete: '/xl/admin/Choice/destroy',
    urlDeleteAll: '/xl/admin/Choice/destroy_many',
    urlCopy: '/xl/admin/Choice/copy',
    urlEditMany: '/xl/admin/Choice/edit_many',
    urlTree: '/xl/admin/Choice/tree',
    urlChoice: '/xl/admin/Choice/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}