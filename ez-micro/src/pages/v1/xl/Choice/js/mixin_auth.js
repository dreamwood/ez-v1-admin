import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.Choice.add", "选项_新增"),//新增
            auth_copy: ezAuth.reg("+xl.Choice.copy", "选项_复制"),//复制
            auth_edit: ezAuth.reg("+xl.Choice.edit", "选项_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.Choice.del", "选项_删除"),//删除
            auth_view: ezAuth.reg("+xl.Choice.view", "选项_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}