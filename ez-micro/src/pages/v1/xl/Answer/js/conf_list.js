import listBuilder from "@/comps/list/listBuilder";

/*
lb.add("title","解析",80)
lb.add("scoreMin","分数下限",80)
lb.add("scoreMax","分数上限",80)

*/
export default function () {
    let lb = listBuilder()
    lb.add("id","#",80)
    lb.addAction(120).setAlignRight()
    return lb.headers
}