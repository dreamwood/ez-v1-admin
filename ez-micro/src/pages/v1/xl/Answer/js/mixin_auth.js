import ezAuth from "@/assets/js/ez-auth";

export default {
    data() {
        return {
            auth_add: ezAuth.reg("+xl.Answer.add", "解析_新增"),//新增
            auth_copy: ezAuth.reg("+xl.Answer.copy", "解析_复制"),//复制
            auth_edit: ezAuth.reg("+xl.Answer.edit", "解析_编辑"),//编辑
            auth_del: ezAuth.reg("-xl.Answer.del", "解析_删除"),//删除
            auth_view: ezAuth.reg("+xl.Answer.view", "解析_查看"),//查看
        }
    },
    directives: {
        auth: {
            bind: ($el, binding, vnode, prevVnode) => {
                ezAuth.checkAllow(binding.value) ? $el.style.display = "normal" : $el.style.display = "none"
            }
        }
    }
}