import http from "@/assets/js/http";

export default {
    urlFind: '/xl/admin/Answer/get',
    urlFindToEdit: '/xl/admin/Answer/get',
    urlFindBy: '/xl/admin/Answer/list',
    urlSave: '/xl/admin/Answer/save',
    //urlDelete: '/xl/admin/Answer/delete',
    //urlDeleteAll: '/xl/admin/Answer/delete_many',
    urlDelete: '/xl/admin/Answer/destroy',
    urlDeleteAll: '/xl/admin/Answer/destroy_many',
    urlCopy: '/xl/admin/Answer/copy',
    urlEditMany: '/xl/admin/Answer/edit_many',
    urlTree: '/xl/admin/Answer/tree',
    urlChoice: '/xl/admin/Answer/choice',
    find(id, func) {
        http.get(this.urlFind, {id}, res => {
            func(res)
        })
    },
    findToEdit(id, func) {
        http.get(this.urlFindToEdit, {id}, res => {
            func(res)
        })
    },
    findBy(param, func) {
        http.post(this.urlFindBy, param, res => {
            func(res.data.lists, res.data.query)
        })
    },
    save(data, func) {
        http.post(this.urlSave, data, res => {
            func(res)
        })
    },
    delete(id, func) {
        http.get(this.urlDelete, {id}, res => {
            func(res)
        })
    },
    deleteAll(ids, func) {
        http.post(this.urlDeleteAll, {ids}, res => {
            func(res)
        })
    },
    copy(ids, func) {
        http.post(this.urlCopy, {ids}, res => {
            func(res)
        })
    },
    editMany(ids, data, func) {
        http.post(this.urlEditMany, {
            ids,
            _action: data
        }, res => {
            func(res)
        })
    },
}