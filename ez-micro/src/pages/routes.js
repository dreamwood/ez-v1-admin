export default [
  {
    "name": "root",
    "path": "/",
    "componentPath": "./layout.vue",
    "children": [
      {
        "name": "NYJSORVGMO",
        "path": "doc",
        "componentPath": "./doc/layout.vue",
        "children": [
          {
            "name": "OEYCVROIUV",
            "path": "models",
            "componentPath": "./doc/models.vue"
          },
          {
            "name": "IBVOYHWLTI",
            "path": "welcome",
            "componentPath": "./doc/welcome.vue"
          }
        ]
      },
      {
        "name": "MBVVSTDTAW",
        "path": "login",
        "componentPath": "./login.vue"
      },
      {
        "name": "AOHQLFPESV",
        "path": "v1",
        "componentPath": "./v1/layout.vue",
        "children": [
          {
            "name": "",
            "path": "core",
            "componentPath": "./v1/core/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Api",
                "componentPath": "./v1/core/Api/layout.vue",
                "children": [
                  {
                    "name": "JNYPYDDLQI",
                    "path": "edit",
                    "componentPath": "./v1/core/Api/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Api/js/layout.vue"
                  },
                  {
                    "name": "HGJUAUTUNI",
                    "path": "list",
                    "componentPath": "./v1/core/Api/list.vue"
                  },
                  {
                    "name": "BXXSQRRROS",
                    "path": "tree",
                    "componentPath": "./v1/core/Api/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "GateWay",
                "componentPath": "./v1/core/GateWay/layout.vue",
                "children": [
                  {
                    "name": "NUPWSULSEP",
                    "path": "edit",
                    "componentPath": "./v1/core/GateWay/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/GateWay/js/layout.vue"
                  },
                  {
                    "name": "HCCUSPGSBN",
                    "path": "list",
                    "componentPath": "./v1/core/GateWay/list.vue"
                  },
                  {
                    "name": "IGCRISIPGH",
                    "path": "tree",
                    "componentPath": "./v1/core/GateWay/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Host",
                "componentPath": "./v1/core/Host/layout.vue",
                "children": [
                  {
                    "name": "CMNPCCRQWN",
                    "path": "edit",
                    "componentPath": "./v1/core/Host/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Host/js/layout.vue"
                  },
                  {
                    "name": "BARMPIGIPE",
                    "path": "list",
                    "componentPath": "./v1/core/Host/list.vue"
                  },
                  {
                    "name": "DHNHXEITQE",
                    "path": "tree",
                    "componentPath": "./v1/core/Host/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Menu",
                "componentPath": "./v1/core/Menu/layout.vue",
                "children": [
                  {
                    "name": "LJKHKMLLTH",
                    "path": "edit",
                    "componentPath": "./v1/core/Menu/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Menu/js/layout.vue"
                  },
                  {
                    "name": "KQOFXYTQNV",
                    "path": "list",
                    "componentPath": "./v1/core/Menu/list.vue"
                  },
                  {
                    "name": "PXODLSQGIY",
                    "path": "tree",
                    "componentPath": "./v1/core/Menu/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Model",
                "componentPath": "./v1/core/Model/layout.vue",
                "children": [
                  {
                    "name": "CVGQAKJGWI",
                    "path": "edit",
                    "componentPath": "./v1/core/Model/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Model/js/layout.vue"
                  },
                  {
                    "name": "AKPLWUIBUV",
                    "path": "list",
                    "componentPath": "./v1/core/Model/list.vue"
                  },
                  {
                    "name": "UREYMLBGJM",
                    "path": "tree",
                    "componentPath": "./v1/core/Model/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Page",
                "componentPath": "./v1/core/Page/layout.vue",
                "children": [
                  {
                    "name": "PXNVWDKEBA",
                    "path": "edit",
                    "componentPath": "./v1/core/Page/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Page/js/layout.vue"
                  },
                  {
                    "name": "SMEGSNLNSH",
                    "path": "list",
                    "componentPath": "./v1/core/Page/list.vue"
                  },
                  {
                    "name": "WNEIARVOAM",
                    "path": "tree",
                    "componentPath": "./v1/core/Page/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "RequestLog",
                "componentPath": "./v1/core/RequestLog/layout.vue",
                "children": [
                  {
                    "name": "JPFLPIVGLU",
                    "path": "edit",
                    "componentPath": "./v1/core/RequestLog/edit.vue"
                  },
                  {
                    "name": "RVUHQUASPH",
                    "path": "info",
                    "componentPath": "./v1/core/RequestLog/info.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/RequestLog/js/layout.vue"
                  },
                  {
                    "name": "TNLCNCKHFB",
                    "path": "list",
                    "componentPath": "./v1/core/RequestLog/list.vue"
                  },
                  {
                    "name": "NGAQOFRGJD",
                    "path": "tree",
                    "componentPath": "./v1/core/RequestLog/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Role",
                "componentPath": "./v1/core/Role/layout.vue",
                "children": [
                  {
                    "name": "HKYPLSSVCE",
                    "path": "edit",
                    "componentPath": "./v1/core/Role/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Role/js/layout.vue",
                    "children": [
                      {
                        "name": "OTUUGGGNSB",
                        "path": "ApiSelector",
                        "componentPath": "./v1/core/Role/js/ApiSelector.vue"
                      },
                      {
                        "name": "VMCCLDQPSV",
                        "path": "PageSelector",
                        "componentPath": "./v1/core/Role/js/PageSelector.vue"
                      },
                      {
                        "name": "RAHCOUBSKL",
                        "path": "PageSelector_bkp",
                        "componentPath": "./v1/core/Role/js/PageSelector_bkp.vue"
                      }
                    ]
                  },
                  {
                    "name": "YPLWUGPDWV",
                    "path": "list",
                    "componentPath": "./v1/core/Role/list.vue"
                  },
                  {
                    "name": "GANKANFJFD",
                    "path": "tree",
                    "componentPath": "./v1/core/Role/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Route",
                "componentPath": "./v1/core/Route/layout.vue",
                "children": [
                  {
                    "name": "LJUCRHTGOH",
                    "path": "edit",
                    "componentPath": "./v1/core/Route/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Route/js/layout.vue"
                  },
                  {
                    "name": "OHTRLDHFIG",
                    "path": "list",
                    "componentPath": "./v1/core/Route/list.vue"
                  },
                  {
                    "name": "BEXKCSVFQB",
                    "path": "tree",
                    "componentPath": "./v1/core/Route/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Top",
                "componentPath": "./v1/core/Top/layout.vue",
                "children": [
                  {
                    "name": "WRKEBSKAXO",
                    "path": "edit",
                    "componentPath": "./v1/core/Top/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/Top/js/layout.vue"
                  },
                  {
                    "name": "GDLOLCPEIC",
                    "path": "list",
                    "componentPath": "./v1/core/Top/list.vue"
                  },
                  {
                    "name": "RSLRTGISVA",
                    "path": "tree",
                    "componentPath": "./v1/core/Top/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "User",
                "componentPath": "./v1/core/User/layout.vue",
                "children": [
                  {
                    "name": "OPDDUCGTAI",
                    "path": "edit",
                    "componentPath": "./v1/core/User/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/core/User/js/layout.vue"
                  },
                  {
                    "name": "WKAAPSWQCG",
                    "path": "list",
                    "componentPath": "./v1/core/User/list.vue"
                  },
                  {
                    "name": "DNBUKUWCNS",
                    "path": "tree",
                    "componentPath": "./v1/core/User/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "",
            "path": "dev",
            "componentPath": "./v1/dev/layout.vue",
            "children": [
              {
                "name": "",
                "path": "EzModel",
                "componentPath": "./v1/dev/EzModel/layout.vue",
                "children": [
                  {
                    "name": "QWUTUHFFOE",
                    "path": "dev",
                    "componentPath": "./v1/dev/EzModel/dev.vue"
                  },
                  {
                    "name": "XSMPTLLHIN",
                    "path": "edit",
                    "componentPath": "./v1/dev/EzModel/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/dev/EzModel/js/layout.vue"
                  },
                  {
                    "name": "QECNGNSEAG",
                    "path": "list",
                    "componentPath": "./v1/dev/EzModel/list.vue"
                  },
                  {
                    "name": "LNAKEWFLYX",
                    "path": "tree",
                    "componentPath": "./v1/dev/EzModel/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "EzModelField",
                "componentPath": "./v1/dev/EzModelField/layout.vue",
                "children": [
                  {
                    "name": "YVBSJUARLH",
                    "path": "edit",
                    "componentPath": "./v1/dev/EzModelField/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/dev/EzModelField/js/layout.vue"
                  },
                  {
                    "name": "UPHOQSFFTT",
                    "path": "list",
                    "componentPath": "./v1/dev/EzModelField/list.vue"
                  },
                  {
                    "name": "GLQYMKCUCN",
                    "path": "tree",
                    "componentPath": "./v1/dev/EzModelField/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "",
            "path": "uc",
            "componentPath": "./v1/uc/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Ucache",
                "componentPath": "./v1/uc/Ucache/layout.vue",
                "children": [
                  {
                    "name": "IVKNNXGUMF",
                    "path": "edit",
                    "componentPath": "./v1/uc/Ucache/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/uc/Ucache/js/layout.vue"
                  },
                  {
                    "name": "WDYJUVRQFE",
                    "path": "list",
                    "componentPath": "./v1/uc/Ucache/list.vue"
                  },
                  {
                    "name": "HNETVSLQWR",
                    "path": "tree",
                    "componentPath": "./v1/uc/Ucache/tree.vue"
                  }
                ]
              }
            ]
          },
          {
            "name": "TRDCLDWPIJ",
            "path": "welcome",
            "componentPath": "./v1/welcome.vue"
          },
          {
            "name": "",
            "path": "xl",
            "componentPath": "./v1/xl/layout.vue",
            "children": [
              {
                "name": "",
                "path": "Answer",
                "componentPath": "./v1/xl/Answer/layout.vue",
                "children": [
                  {
                    "name": "JGGNTIQHXW",
                    "path": "edit",
                    "componentPath": "./v1/xl/Answer/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Answer/js/layout.vue"
                  },
                  {
                    "name": "MJIVLUWRLW",
                    "path": "list",
                    "componentPath": "./v1/xl/Answer/list.vue"
                  },
                  {
                    "name": "GVMCLEAFMA",
                    "path": "tree",
                    "componentPath": "./v1/xl/Answer/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Choice",
                "componentPath": "./v1/xl/Choice/layout.vue",
                "children": [
                  {
                    "name": "GYUCQTCIER",
                    "path": "edit",
                    "componentPath": "./v1/xl/Choice/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Choice/js/layout.vue"
                  },
                  {
                    "name": "IUVXCRJNPN",
                    "path": "list",
                    "componentPath": "./v1/xl/Choice/list.vue"
                  },
                  {
                    "name": "WVJXQTIVVS",
                    "path": "tree",
                    "componentPath": "./v1/xl/Choice/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Question",
                "componentPath": "./v1/xl/Question/layout.vue",
                "children": [
                  {
                    "name": "DLNGVRXKHE",
                    "path": "edit",
                    "componentPath": "./v1/xl/Question/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Question/js/layout.vue"
                  },
                  {
                    "name": "VPNKRRHGIF",
                    "path": "list",
                    "componentPath": "./v1/xl/Question/list.vue"
                  },
                  {
                    "name": "BGAMNGICHG",
                    "path": "tree",
                    "componentPath": "./v1/xl/Question/tree.vue"
                  }
                ]
              },
              {
                "name": "",
                "path": "Test",
                "componentPath": "./v1/xl/Test/layout.vue",
                "children": [
                  {
                    "name": "JEBSOUILWR",
                    "path": "edit",
                    "componentPath": "./v1/xl/Test/edit.vue"
                  },
                  {
                    "name": "",
                    "path": "js",
                    "componentPath": "./v1/xl/Test/js/layout.vue"
                  },
                  {
                    "name": "UQKVEFMNTH",
                    "path": "list",
                    "componentPath": "./v1/xl/Test/list.vue"
                  },
                  {
                    "name": "VTJLXOLLTA",
                    "path": "tree",
                    "componentPath": "./v1/xl/Test/tree.vue"
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
]